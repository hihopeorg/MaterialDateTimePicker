# Material DateTime Picker - 多样式的时间/日期选择器

**本项目是基于开源项目MaterialDateTimePicker进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/wdullaer/MaterialDateTimePicker ）追踪到原项目版本**

## 项目介绍

- 项目名称：MaterialDateTimePicker
- 所属系列：ohos的第三方组件适配移植
- 功能：多样式的时间/日期选择器。
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/wdullaer/MaterialDateTimePicker
- 原项目基线版本：v4.2.3, sha1:f849a5c2704c974ba182fe4e2e205fa7f4fd395d
- 编程语言：Java
- 外部库依赖：无

##### 效果展示

- TimePicker

![p1](picture/TimePicker.gif)

- DatePicker

![p1](picture/DatePicker.gif)

## 安装教程

方法1.

1. 下载har包MaterialDateTimePicker.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.wdullaer.ohos:materialdatetimepicker:1.0.1'
}
```



## 使用说明

### 使用 Material Date/Time Pickers
该库沿用了一些与Android Framework相同的API。
基本实现需要：

1. 实现 `OnTimeSetListener`/`OnDateSetListener`
2. 使用提供的factory来创建 `TimePickerDialog`/`DatePickerDialog`
3. 设置选择器的主题

#### 实现 `OnTimeSetListener`/`OnDateSetListener`
为了接收选择器的时间或日期选择结果，需要实现`OnTimeSetListener`或`OnDateSetListener`接口。
一般情况下，我们可以在`Ability` 或 `AbilitySlice` 中创建Pickers。
```java
@Override
public void void onTimeSet(TimePickerDialog dialog, int hourOfDay, int minute, int second) {
  String time = "You picked the following time: "+hourOfDay+"h"+minute+"m"+second;
  timeTextView.setText(time);
}

@Override
public void void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
  String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
  dateTextView.setText(date);
}
```

#### 使用提供的factory创建`TimePickerDialog`/`DatePickerDialog`
可以使用一个静态方法`newInstance()`来创建 `TimePickerDialog` 或 `DatePickerDialog` 的实例, 该方法提供合适的默认值和回调。
当dialog配置完成后，可以调用 `show()` 方法来显示.
```java
Calendar now = Calendar.getInstance();
DatePickerDialog dpd = DatePickerDialog.newInstance(
  MainActivity.this,
  now.get(Calendar.YEAR), // Initial year selection
  now.get(Calendar.MONTH), // Initial month selection
  now.get(Calendar.DAY_OF_MONTH) // Inital day selection
);
dpd.show();
```

#### 设置选择器主题
该库为每个选择器提供了2个版本的布局

* 版本 1: 一般布局样式.最新的material设计风格。
* 版本 2: marshmallow版本样式. 当前的默认设计。

可以使用工厂方法来设置布局版本：
```java
dpd.setVersion(DatePickerDialog.Version.VERSION_2);
```

选择器将基于当前创建的主题，使用`colorAccent`属性配置主题色。也可以使用 `setAccentColor(int color)` 方法来设置主题色。
同样，也可以通过覆盖项目中颜色资源的 `mdtp_accent_color` 和 `mdtp_accent_color_dark`。
```xml
<color name="mdtp_accent_color">#009688</color>
<color name="mdtp_accent_color_dark">#00796b</color>
```

颜色选择优先级规定如下：

1. Java代码中设置`setAccentColor(int color)`
2. 工程中资源文件中定义的`mdtp_accent_color` 和 `mdtp_accent_color_dark`


### 其他选项
#### [All] `setThemeDark(boolean themeDark)`
Dialog可以通过调用下面方法来设置dark主题
```java
dialog.setThemeDark(true);
```

#### [All] `setAccentColor(String color)` 和 `setAccentColor(int color)`
给Dialog设置accentColor。

#### [All] `setOkColor()` 和 `setCancelColor()`
设置OK或Cancel按钮的文字颜色。类似于 `setAccentColor()`

#### [TimePickerDialog] `setTitle(String title)`
在`TimePickerDialog`顶部显示标题

#### [DatePickerDialog] `setTitle(String title)`
在`DatePickerDialog`顶部显示标题，而不是显示星期几

#### [All] `setOkText()` 和 `setCancelText()`
设置自定义的Ok 和 Cancel按钮的文字，也可以设置资源ID。

#### [DatePickerDialog] `setMinDate(Calendar day)`
这只最小可以选择的有效日期。

#### [DatePickerDialog] `setMaxDate(Calendar day)`
设置最大可以选择的有效日期。

#### [TimePickerDialog] `setMinTime(Timepoint time)`
设置最小可以选择的有效时间。

#### [TimePickerDialog] `setMaxTime(Timepoint time)`
设置最大可以选择的有效时间。

#### [TimePickerDialog] `setSelectableTimes(Timepoint[] times)`
设置可选择的时间点。

#### [TimePickerDialog] `setDisabledTimes(Timepoint[] times)`
设置禁用的时间点。

#### [TimePickerDialog] `setTimeInterval(int hourInterval, int minuteInterval, int secondInterval)`
设置时间间隔。

#### [TimePickerDialog] `setTimepointLimiter(TimepointLimiter limiter)`
设置事件限制器。

#### [DatePickerDialog] `setSelectableDays(Calendar[] days)`
设置可选择的日期列表。

#### [DatePickerDialog] `setDisabledDays(Calendar[] days)`
设置禁用的日期。

#### [DatePickerDialog] `setHighlightedDays(Calendar[] days)`
设置高亮日期。

#### [DatePickerDialog] `showYearPickerFirst(boolean yearPicker)`
优先显示年。

#### [All] `OnDismissListener` 和 `OnCancelListener`
两种选择器都可以设置 `DialogInterface.OnDismissLisener` 或 `DialogInterface.OnCancelListener`来监听相关事件：
```java
tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
    @Override
    public void onCancel(DialogInterface dialogInterface) {
      Log.d("TimePicker", "Dialog was cancelled");
    }
});
```

#### [All] `vibrate(boolean vibrate)`
设置当选择时是否震动，默认为true。

#### [All] `dismissOnPause(boolean dismissOnPause)`
设置当页面暂停或重新创建是，选择器是否消失。

#### [All] `setLocale(Locale locale)`
允许客户端设置在选择器中生成各种字符串时将使用的自定义语言环境。 默认情况下，将使用设备的当前区域设置。 因为选择器默认情况下将适应设备的语言环境，所以您只需要在非常罕见的情况下使用它。

#### [DatePickerDialog] `autoDismiss(boolean autoDismiss)`
如果设置为“ true”，则当用户选择日期时将关闭选择器。 默认为“ false”。

#### [TimepickerDialog] `enableSeconds(boolean enableSconds)` and `enableMinutes(boolean enableMinutes)`
允许您在“ TimepickerDialog”上启用或禁用秒和分钟选择器。 启用秒选择器，意味着启用分钟选择器。 禁用分钟选择器将禁用秒选择器。 将使用最后一次应用的设置。 默认情况下，“ enableSeconds = false”和“ enableMinutes = true”。

#### [DatePickerDialog] `setTimeZone(Timezone timezone)` *deprecated*
设置“时区”，用于在选择器内部表示时间。 默认为设备的当前默认时区。
不建议使用此方法：您应该使用`newInstance（）`方法，该方法将Calendar设置为适当的TimeZone。

#### [DatePickerDialog] `setDateRangeLimiter(DateRangeLimiter limiter)`
提供DateRangeLimiter的自定义实现，使您可以完全控制哪些日期可供选择。 这将禁用所有其他限制日期选择的选项。

#### `getOnTimeSetListener()` 和 `getOnDateSetListener()`
允许检索对当前与选择器相关联的回调的引用的获取器

#### [DatePickerDialog] `setScrollOrientation(ScrollOrientation scrollOrientation)` 和 `getScrollOrientationi()`
确定月份是滚动“水平”还是“垂直”。 对于v2版式，默认为`Horizontal`；对于v1版式，默认为`Vertical`。

## FAQ

###为什么“ DatePickerDialog”返回所选的月份-1？
在Java日历类的月份中，使用基于0的索引：一月是月份0，十二月是月份11。该约定在Java世界中被广泛使用，例如，本机ohos DatePicker。

###如何将其变成一年和一个月的选择器？
这个DatePickerDialog专注于选择日期，这意味着它的主要设计元素是日期选择器。由于这种类似于日历的视图是设计的中心，因此尝试禁用它是没有意义的。因此，仅选择年份和月份（没有一天）就不在此库的范围内，因此不会添加。

###如何选择多天？
该库的目标是实现“材料设计日期”选择器。此设计的重点是精确选择1个日期（顶部带有大文本表示）。需要进行大量重新设计，才能选择多个工作日。因此，此功能当前不在该库的范围内，因此不会添加。

###如何使用自定义逻辑启用/禁用日期？
“ DatePickerDialog”公开了一些实用程序方法来启用/禁用常见场景的日期。如果这些不能满足您的需求，则可以提供“ DateRangeLimiter”接口的自定义实现。
由于在对话框对话框中保留了“ DateRangeLimiter”，因此您的实现还必须实现序列化打包接口。

```java
class MyDateRangeLimiter implements DateRangeLimiter {
  public MyDateRangeLimiter(Parcel in) {

  }

  @Override
  public int getMinYear() {
    return 1900;
  }

  @Override
  public int getMaxYear() {
    return 2100;
  }

  @Override
  public Calendar getStartDate() {
    Calendar output = Calendar.newInstance();
    output.set(Calendar.YEAR, 1900);
    output.set(Calendar.DAY_OF_MONTH, 1);
    output.set(Calendar.MONTH, Calendar.JANUARY);
    return output;
  }

  @Override
  public Calendar getEndDate() {
    Calendar output = Calendar.newInstance();
    output.set(Calendar.YEAR, 2100);
    output.set(Calendar.DAY_OF_MONTH, 1);
    output.set(Calendar.MONTH, Calendar.JANUARY);
    return output;
  }

  @Override
  public boolean isOutOfRange(int year, int month, int day) {
    return false;
  }

  @Override
  public Calendar setToNearestDate(Calendar day) {
      return day;
  }

  @Override
  public void writeToParcel(Parcel out) {

  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Sequenceable.Producer<DefaultDateRangeLimiter> CREATOR
        = new Sequenceable.Producer<DefaultDateRangeLimiter>() {
    public DefaultDateRangeLimiter createFromParcel(Parcel in) {
        return new DefaultDateRangeLimiter(in);
    }

    public DefaultDateRangeLimiter[] newArray(int size) {
        return new DefaultDateRangeLimiter[size];
    }
  };
}
```

当使用自定义的`DateRangeLimiter`时，用于设置启用/禁用日期的内置方法将不再起作用。 你需要自己实现该功能。

###当设备更改方向时，为什么我的回调丢失了？
一种简单的解决方案是在您的活动暂停时dismiss选择器。

```java
tpd.dismissOnPause(true);
```

如果希望在方向发生变化时保留选择器，可能不太容易。

默认情况下，当方向发生变化时，ohos会销毁并重新创建`Ability`。 该库将尽可能在方向更改时保留其状态。
唯一值得注意的例外是不同的回调和侦听器。 这些接口通常在`Ability`或`AbilitySlice`上实现。
如果尝试保留它们会导致内存泄漏。 除了明确要求在Ability上实现回调接口之外，目前看没有一种安全的方法可以正确保留回调。

你需要在Ability的onActive()回调中设置侦听器。

```java
@Override
public void onActive() {
  super.onActive();
  if(tpd != null) tpd.setOnTimeSetListener(this);
  if(dpd != null) dpd.setOnDateSetListener(this);
}
```


## 版本迭代

- v1.0.1

## 版权和许可信息

    Copyright (c) 2015 Wouter Dullaert
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

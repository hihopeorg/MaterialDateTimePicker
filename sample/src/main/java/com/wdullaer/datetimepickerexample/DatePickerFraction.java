package com.wdullaer.datetimepickerexample;

import com.wdullaer.materialdatetimepicker.common.DialogBase;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.Calendar;

/**
 * A simple {@link Fraction} subclass.
 */
public class DatePickerFraction extends Fraction implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = "MonthFragment";
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 1234567, TAG);

    private Text dateTextView;
    private Checkbox modeDarkDate;
    private Checkbox modeCustomAccentDate;
    private Checkbox vibrateDate;
    private Checkbox dismissDate;
    private Checkbox titleDate;
    private Checkbox showYearFirst;
    private Checkbox showVersion2;
    private Checkbox switchOrientation;
    private Checkbox limitSelectableDays;
    private Checkbox highlightDays;
    private Checkbox defaultSelection;
    private DatePickerDialog dpd;

    public DatePickerFraction() {
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_datepicker_layout, container, false);

        // Find our Component instances
        dateTextView = (Text)component.findComponentById(ResourceTable.Id_date_textview);
        Button dateButton = (Button)component.findComponentById(ResourceTable.Id_date_button);
        modeDarkDate = (Checkbox)component.findComponentById(ResourceTable.Id_mode_dark_date);
        modeCustomAccentDate = (Checkbox)component.findComponentById(ResourceTable.Id_mode_custom_accent_date);
        vibrateDate = (Checkbox)component.findComponentById(ResourceTable.Id_vibrate_date);
        dismissDate = (Checkbox)component.findComponentById(ResourceTable.Id_dismiss_date);
        titleDate = (Checkbox)component.findComponentById(ResourceTable.Id_title_date);
        showYearFirst = (Checkbox)component.findComponentById(ResourceTable.Id_show_year_first);
        showVersion2 = (Checkbox)component.findComponentById(ResourceTable.Id_show_version_2);
        switchOrientation = (Checkbox)component.findComponentById(ResourceTable.Id_switch_orientation);
        limitSelectableDays = (Checkbox)component.findComponentById(ResourceTable.Id_limit_dates);
        highlightDays = (Checkbox)component.findComponentById(ResourceTable.Id_highlight_dates);
        defaultSelection = (Checkbox)component.findComponentById(ResourceTable.Id_default_selection);

        component.findComponentById(ResourceTable.Id_original_button).setClickedListener(c -> {
            CommonDialog dialog = new DialogBase(c.getContext());
            ohos.agp.components.DatePicker datePicker = new ohos.agp.components.DatePicker(c.getContext());
            datePicker.setLayoutConfig(new ComponentContainer.LayoutConfig(800, 400));
            datePicker.setSelectedTextSize(40);
            datePicker.setNormalTextSize(30);
            datePicker.setSelectedTextColor(new Color(Color.getIntColor("#FFA500")));
            datePicker.setOperatedTextColor(new Color(Color.getIntColor("#00FFFF")));
            datePicker.setWheelModeEnabled(true);
            ShapeElement shape = new ShapeElement();
            shape.setShape(ShapeElement.RECTANGLE);
            shape.setRgbColor(RgbColor.fromArgbInt(0xFF9370DB));
            datePicker.setDisplayedLinesElements(shape,shape);
            dialog.setAlignment(LayoutAlignment.CENTER);
            dialog.setContentCustomComponent(datePicker);
            dialog.show();
        });

        // Show a datepicker when the dateButton is clicked
        dateButton.setClickedListener(c -> {
            Calendar now = Calendar.getInstance();
            if (defaultSelection.isChecked()) {
                now.add(Calendar.DATE, 7);
            }
            /*
            It is recommended to always create a new instance whenever you need to show a Dialog.
            The sample app is reusing them because it is useful when looking for regressions
            during testing
             */
            HiLog.warn(LABEL, "### on dateButton click");
            if (dpd == null) {
                HiLog.warn(LABEL, "### dpd == null");
                dpd = DatePickerDialog.newInstance(
                        container.getContext(),
                        DatePickerFraction.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
            } else {
                HiLog.warn(LABEL, "### dpd != null");
                dpd.initialize(
                        DatePickerFraction.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
            }
            dpd.setAlignment(LayoutAlignment.CENTER);
            dpd.setThemeDark(modeDarkDate.isChecked());
            dpd.vibrate(vibrateDate.isChecked());
            dpd.dismissOnPause(dismissDate.isChecked());
            dpd.showYearPickerFirst(showYearFirst.isChecked());
            HiLog.warn(LABEL, "### showVersion2.isChecked():" +showVersion2.isChecked());
            dpd.setVersion(showVersion2.isChecked() ? DatePickerDialog.Version.VERSION_2 : DatePickerDialog.Version.VERSION_1);
            if (modeCustomAccentDate.isChecked()) {
                dpd.setAccentColor(Color.getIntColor("#9C27B0"));
            }
            if (titleDate.isChecked()) {
                dpd.setTitle("DatePicker Title");
            }
            if (highlightDays.isChecked()) {
                Calendar date1 = Calendar.getInstance();
                Calendar date2 = Calendar.getInstance();
                date2.add(Calendar.WEEK_OF_MONTH, -1);
                Calendar date3 = Calendar.getInstance();
                date3.add(Calendar.WEEK_OF_MONTH, 1);
                Calendar[] days = {date1, date2, date3};
                dpd.setHighlightedDays(days);
            }
            if (limitSelectableDays.isChecked()) {
                Calendar[] days = new Calendar[13];
                for (int i = -6; i < 7; i++) {
                    Calendar day = Calendar.getInstance();
                    day.add(Calendar.DAY_OF_MONTH, i * 2);
                    days[i + 6] = day;
                }
                dpd.setSelectableDays(days);
            }
            if (switchOrientation.isChecked()) {
                if (dpd.getVersion() == DatePickerDialog.Version.VERSION_1) {
                    dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.HORIZONTAL);
                } else {
                    dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                }
            }
            dpd.setOnCancelListener(dialog -> {
                HiLog.warn(LABEL, "Dialog was cancelled");
                dpd = null;
            });
//            dpd.show(requireFragmentManager(), "Datepickerdialog");
            dpd.show();
        });

        return component;
    }

    @Override
    protected void onStop() {
        super.onStop();
        dpd = null;
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        if(dpd != null) {
            dpd.onBackground(dpd);
            dpd = null;
        }
    }

    @Override
    protected void onActive() {
        super.onActive();
//        DatePickerDialog dpd = (DatePickerDialog) requireFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: "+dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        dateTextView.setText(date);
        dpd = null;
    }
}

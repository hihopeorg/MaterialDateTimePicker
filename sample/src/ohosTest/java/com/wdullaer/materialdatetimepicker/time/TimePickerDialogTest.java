package com.wdullaer.materialdatetimepicker.time;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

public class TimePickerDialogTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void getPickerResolutionShouldReturnSecondIfSecondsAreEnabled() {
        Context context = sAbilityDelegator.getAppContext();
        TimePickerDialog tpd = TimePickerDialog.newInstance(context, null, false);
        tpd.enableSeconds(true);
        Assert.assertEquals(tpd.getPickerResolution(), Timepoint.TYPE.SECOND);
    }

    @Test
    public void getPickerResolutionShouldReturnMinuteIfMinutesAreEnabled() {
        Context context = sAbilityDelegator.getAppContext();
        TimePickerDialog tpd = TimePickerDialog.newInstance(context, null, false);
        tpd.enableSeconds(false);
        tpd.enableMinutes(true);
        Assert.assertEquals(tpd.getPickerResolution(), Timepoint.TYPE.MINUTE);
    }

    @Test
    public void getPickerResolutionShouldReturnHourIfMinutesAndSecondsAreDisabled() {
        Context context = sAbilityDelegator.getAppContext();
        TimePickerDialog tpd = TimePickerDialog.newInstance(context, null, false);
        tpd.enableMinutes(false);
        Assert.assertEquals(tpd.getPickerResolution(), Timepoint.TYPE.HOUR);
    }
}
